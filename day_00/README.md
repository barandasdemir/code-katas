# September 9th 2019

# 7 kyu

* [disemvowel-trolls](https://www.codewars.com/kata/52fba66badcd10859f00097e)
* [jaden-casing-strings](https://www.codewars.com/kata/jaden-casing-strings)

# 6 kyu

* [who-likes-it](https://www.codewars.com/kata/who-likes-it)
* [fun-with-lists-filter](https://www.codewars.com/kata/fun-with-lists-filter)

# 5 kyu

* [calculating-with-functions](https://www.codewars.com/kata/525f3eda17c7cd9f9e000b39)
