# what's this?

this repo contains solutions to various katas from codewars.
**_my_** solutions, btw.

kata links can be found in each day's readme.

# table of katas

### [day 00](./day_00)
| 7 kyu | 6 kyu | 5 kyu |
|:-----:|:-----:|:-----:|
|[disemvowel-trolls](./day_00/disemvowel-trolls.js)|[who-likes-it](./day_00/who-likes-it.js)|[calculating-with-functions](./day_00/calculating-with-functions.js)|
|[jaden-casing-strings](./day_00/jaden-casing-strings.js)|[fun-with-lists-filter](./day_00/fun-with-lists-filter.js)||
